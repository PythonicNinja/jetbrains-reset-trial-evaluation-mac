# The script resets JetBrains products trial period but keeps settings, logs and cache

Supports: 

Intellij IDEA

WebStorm

DataGrip

PhpStorm

CLion

PyCharm

RubyMine

Rider

GoLand

## Please use it in study purpose only!

## Usage

Run script:

 ```sh
 ./runme.sh
```

Based on https://github.com/alexatiks/jetbrains-reset-trial-evaluation-mac

Improved by removing requirement for reboot and automated closing all windows.
